These are the source files to accompany the SINDy algorithm video on the YouTube channel ThatMathThing (http://www.thatmaththing.com/) by Joel A. Rosenfeld

Everything is included except for the trajectory data. That can be generated with the rk4method function.

If you use this code, please cite the paper https://arxiv.org/pdf/1909.11792.pdf as well as the YouTube video.