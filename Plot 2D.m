% Code by Joel A. Rosenfeld%Plot 2D Approximations
% Cite https://arxiv.org/pdf/1909.11792.pdf if used.

MyApproximation = @(x) (Weights'*MonomialBasisND(x))';

x = MinimumW(1):0.05:MaximumW(1);
y = MinimumW(2):0.05:MaximumW(2);

[X,Y] = meshgrid(x,y);

Z1 = zeros(size(X));

Z2 = zeros(size(X));

for i=1:length(x)
    for j = 1:length(y)
        ApproxPoint = MyApproximation([X(j,i);Y(j,i)]);
        Z1(j,i) = ApproxPoint(1);
        Z2(j,i) = ApproxPoint(2);
    end
end

figure
surf(X,Y,Z1);
title('Dimension 1 Approximation');

figure
surf(X,Y,Z2);
title('Dimension 2 Approximation');

%Calculate Errors
f = @(x) [ x(2); x(1) - x(1)^3];  %Duffing Oscillator


x = MinimumW(1):0.05:MaximumW(1);
y = MinimumW(2):0.05:MaximumW(2);

[X,Y] = meshgrid(x,y);

errZ1 = zeros(size(X));
errZ2 = zeros(size(X));

for i=1:length(x)
    for j = 1:length(y)
        HoldMe = abs(MyApproximation([X(j,i);Y(j,i)]) - f([X(j,i);Y(j,i)]));
        errZ1(j,i) = HoldMe(1);
        errZ2(j,i) = HoldMe(2);
    end
end

figure
surf(X,Y,errZ1);
title('First Dimension Error');

figure
surf(X,Y,errZ2);
title('Second Dimension Error');

toc
